package Model;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.apache.catalina.User;

import java.sql.*;
import Model.Entity.users.Users;

public class UsersModel {
	
	public List<Users> listUsers(DataSource dataSource) {
		
		
		List<Users> listUsers = new ArrayList<>();
		
		Connection connect = null;
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		connect = dataSource.getConnection();
    	
    		String query = "Select * from users";
    		stmt = connect.createStatement();
    		
    		rs = stmt.executeQuery(query);
    		
    		while(rs.next()){
    			listUsers.add(new Users(rs.getString("username"),rs.getString("password")));		
    		}
    	}
    	
    	catch(SQLException e){
    	
    	e.printStackTrace();	
    	
    	}

		return listUsers;
	}

	
	public boolean AddUser(DataSource dataSource, Users newuser) {
		
		Connection connect = null;
    	
    	ResultSet rs = null;
    	PreparedStatement stmt = null;
		
    	try {
			connect = dataSource.getConnection();
			String username = newuser.getUsername();
			String password = newuser.getPassword();
			
			String query = "insert into users(username,password) values(?,?)";
			stmt = connect.prepareStatement(query);
			stmt.setString(1, username);
			stmt.setString(2, password);
			return stmt.execute();
 			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	
    	
	} 
	
	
}
