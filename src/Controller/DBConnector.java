package Controller;

import java.beans.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.sql.*;
/**
 * Servlet implementation class DBConnector
 */
@WebServlet("/DBConnector")
public class DBConnector extends HttpServlet {
	private static final long serialVersionUID = 1L;
    @Resource(name="jdbc/project")

	private DataSource dataSource;  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DBConnector() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	PrintWriter out = response.getWriter();
    	Connection connect = null;
    	java.sql.Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		connect = dataSource.getConnection();
    	
    		String query = "Select * from users";
    		stmt = connect.createStatement();
    		rs = stmt.executeQuery(query);
    		
    		while(rs.next()){
    			out.print(rs.getString("username"));
    		}
    	}
    	catch(SQLException e) {
    	e.printStackTrace();	
    	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
