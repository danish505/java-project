package Controller;

import java.io.IOException;
//import java.sql.Connection;
//import java.sql.DriverManager;
import javax.sql.*;
import com.mysql.jdbc.Connection;
import Model.UsersModel;
import Model.Entity.users.Users;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.Driver;

/**
 * Servlet implementation class HomeController
 */
@WebServlet("/HomeController")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    @Resource(name="jdbc/project")
    private DataSource dataSource;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String action = request.getParameter("action");
		
		switch(action){
			case "home":
				getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
				break;
			case "about":
				getServletContext().getRequestDispatcher("/about.jsp").forward(request, response);
				break;
			case "contact":
				getServletContext().getRequestDispatcher("/portfolio.jsp").forward(request,response);
				break;
			case "login":
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
				break;
			case "register":
				getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
				break;
			case "insert":
				Users newuser = new Users(request.getParameter("uname"),request.getParameter("password"));
				addUser(newuser);
			default:
				getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
				break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String action = request.getParameter("action");
		if(action.equals("insert")) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			try {
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}		
	}
	
	public void addUser(Users newuser){
		new UsersModel().AddUser(dataSource, newuser);
		
	}
	
 
	
	
	

}
