package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import Model.UsersModel;
import Model.Entity.users.Users;
 
/**
 * Servlet implementation class DashboardController
 */

@WebServlet("/DashboardController")
public class DashboardController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   @Resource(name="jdbc/project")
    private DataSource dataSource;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashboardController() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
    	String action= request.getParameter("action");
		action = action.toLowerCase();
		
		if(action.equals("admin")) {
			getServletContext().getRequestDispatcher("/dashboard.jsp").forward(request, response);
		}
		if(action.equals("users")) {
			List<Users> listUsers = new ArrayList<>();
			listUsers = new UsersModel().listUsers(dataSource);
			if(listUsers.size() > 0) {
			request.setAttribute("listUsers", listUsers);
			getServletContext().getRequestDispatcher("/listUsers.jsp").forward(request, response);
			}
			else {
				getServletContext().getRequestDispatcher("/listUsers.jsp").forward(request,response);
			}
		
			}
		
		if(action.equals("logout")) {
			request.getSession().invalidate();
			getServletContext().getRequestDispatcher("/login.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String action = request.getParameter("action");
		
		
		// checking action
		
		if(action.equals("postlogin")) {
		
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			username = username.toLowerCase();
//			password = password.
			 PrintWriter out = response.getWriter();
			 
 			 
			if(username.equals("dany") && password.equals("admin")) {
			// creating session after checking
			HttpSession session = request.getSession(true);
			session.setMaxInactiveInterval(300);
			session.setAttribute("username", username);
			// redirecting
			response.sendRedirect(request.getContextPath()+"/DashboardController?action=admin");
			}
			else {
			response.sendRedirect(request.getContextPath()+"/HomeController?action=login");
				}
			}
		
		}

}
